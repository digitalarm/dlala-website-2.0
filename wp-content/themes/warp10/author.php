<?php
if ( isset( $_GET['author_name'] ) ) {
	$user = get_userdatabylogin( $author_name );
} else {
	$user = get_userdata( intval( $author ) );
}

get_header(); 
?>

<div class="page-author spacing-inside">
	<div class="container">
		<h1><?php _e( 'Profile', 'w10' ); ?></h1>
		<div class="author-image">
			<?php echo get_avatar( $user->ID, 120 ); ?>
		</div>
		<div class="author-content">
			<div class="author-title">
				<span class="author-name"><?php echo $user->display_name; ?></span><?php if ( $current_user->ID == $user->ID ): ?> - <a href="<?php echo get_edit_profile_url(); ?>">[<?php _e( 'Edit', 'w10' ); ?>]</a><?php endif; ?>
			</div>
			<?php if ( ! empty( $user->description ) ): echo '<p>' . $user->description . '</p>'; endif; ?>
		</div>
	</div>
</div>

<?php
get_footer();
?>
