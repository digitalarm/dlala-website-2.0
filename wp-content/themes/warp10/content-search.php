<?php $cats = get_the_category(); ?>
<div class="post-item<?php foreach( $cats as $cat ) : echo ' post-cat-' . $cat->slug; endforeach; ?>" id="post-<?php the_ID(); ?>">
	<div class="post-item-inner">
		<?php if ( has_post_thumbnail() ) : $url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' ); ?>
			<a class="post-item-image" href="<?php the_permalink(); ?>"><span class="cover" style="background-image: url(<?php echo $url[ 0 ]; ?>);"></span></a>
		<?php endif; ?>
		<div class="post-item-text">
			<a class="post-item-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			<?php if ( get_the_excerpt() ) : ?>
				<div class="post-item-excerpt">
					<?php the_excerpt(); ?>
				</div>
			<?php elseif ( $metadesc = get_metadesc( get_the_ID() ) ) : ?>
				<div class="post-item-excerpt">
					<?php echo $metadesc; ?>
				</div>
			<?php endif; ?>
			<?php if ( get_the_category() ) : ?>
				<span class="post-item-info"><?php _e( 'Posted in ', 'w10' ); the_category( ', ' ); ?></span>
			<?php endif; ?>
		</div>
	</div>
</div>
