/* global WOW */

jQuery( function( $ ) {

	// Text slider
	if ( $().slick ) {

		$( '.text-slides' ).slick({
			'speed': 1500,
			'draggable': false,
			'swipeToSlide': true,
			'autoplay': true,
			'autoplaySpeed': 7000,
			'dots': true,
			'arrows': false,
			'adaptiveHeight': true,
		});

	}

	// Image slider
	if ( $().slick ) {

		$( '.image-slider' ).find( '.image-slides' ).slick({
			'speed': 1500,
			'draggable': false,
			'swipeToSlide': true,
			'autoplay': true,
			'autoplaySpeed': 5000,
			'arrows': false,
		});

		$( '.image-slider-with-nav' ).each( function( k ) {

			var $this = $( this );

			$this.attr( 'id', 'image-slider-' + k );
			$( $( '.image-slider-nav' )[k] ).attr( 'id', 'image-slider-nav-' + k );
			$this.find( '.image-slides' ).slick({
				'speed': 1500,
				'draggable': false,
				'swipeToSlide': true,
				'autoplay': true,
				'autoplaySpeed': 5000,
				'arrows': false,
				'asNavFor': '#image-slider-nav-' + k + ' .nav-slides',
			});

			$( '#image-slider-nav-' + k ).find( '.nav-slides' ).slick({
				'slidesToShow': 5,
				'speed': 1500,
				'draggable': false,
				'swipeToSlide': true,
				'autoplay': true,
				'autoplaySpeed': 5000,
				'centerMode': true,
				'focusOnSelect': true,
				'arrows': false,
				'asNavFor': '#image-slider-' + k + ' .image-slides',
			});

		});


		$( '.client-slider-with-nav' ).each( function( k ) {

			var $this = $( this );

			$this.attr( 'id', 'client-slider-' + k );
			$( $( '.client-slider-nav' )[k] ).attr( 'id', 'client-slider-nav-' + k );
			$this.find( '.image-slides' ).slick({
				'speed': 1500,
				'draggable': false,
				'swipeToSlide': true,
				'autoplay': false,
				'autoplaySpeed': 5000,
				'arrows': false,
				'fade': true,
				'adaptiveHeight': true,
				'pauseOnHover': true,
				'asNavFor': '#client-slider-nav-' + k + ' .nav-slides',
			});

			$( '#client-slider-nav-' + k ).find( '.nav-slides' ).slick({
				'speed': 1500,
				'draggable': false,
				'swipeToSlide': true,
				'arrows': false,
				'autoplay': false,
				'infinite': true,
				'focusOnSelect': true,
				'variableWidth': true,
				'centerMode': true,
	  			'slidesToShow': 4,
	  			'slidesToScroll': 1,
				'asNavFor': '#client-slider-' + k + ' .image-slides',
			});

		});

	}

	// Video slider
	if ( $().slick ) {

		$( '.video-slider' ).find( '.video-slides' ).slick({
			'speed': 1500,
			'draggable': false,
			'swipeToSlide': true,
			'autoplay': true,
			'autoplaySpeed': 5000,
		});

		$( '.video-slider-with-nav' ).each( function( k ) {

			var $this = $( this );

			$this.attr( 'id', 'video-slider-' + k );
			$( $( '.video-slider-nav' )[k] ).attr( 'id', 'video-slider-nav-' + k );
			$this.find( '.video-slides' ).slick({
				'speed': 1500,
				'draggable': false,
				'swipeToSlide': true,
				'autoplay': true,
				'autoplaySpeed': 5000,
				'asNavFor': '#video-slider-nav-' + k + ' .nav-slides',
			});

			$( '#video-slider-nav-' + k ).find( '.nav-slides' ).slick({
				'slidesToShow': 3,
				'speed': 1500,
				'draggable': false,
				'swipeToSlide': true,
				'autoplay': true,
				'autoplaySpeed': 5000,
				'centerMode': true,
				'focusOnSelect': true,
				'asNavFor': '#video-slider-' + k + ' .video-slides',
			});

		});

	}

	$( '.video-slide-play' ).on( 'click', function() {

		$( this ).parent().siblings( '.video-container' ).css( 'visibility', 'visible' );

	});

	// Slideshow slider
	if ( $().slick ) {

		$( '.slideshow-slides' ).slick({
			'speed': 1500,
			'draggable': false,
			'swipeToSlide': true,
			'autoplay': true,
			'autoplaySpeed': 5000,
			'arrows': false,
			'dots': true,
		});

	}

	// Gallery grid
	if ( $().magnificPopup ) {

		$( '.gi-inner' ).magnificPopup({
			'type': 'image',
			'gallery': { 'enabled': true },
		});

	}

	// Carousel slider
	if ( $().slick ) {

		$( '.carousel-slides' ).slick({
			'speed': 1500,
			'swipeToSlide': true,
			'autoplay': true,
			'autoplaySpeed': 5000,
			'arrows': false,
			'slidesToShow': 2,
			'slidesToScroll': 1,
		});

	}

	// WOW
	if ( typeof WOW === 'function' ) {

		new WOW().init();

	}

	// Collapsible blocks
	$( '.collapsible-title' ).on( 'click', function() {

		var title = $( this );

		$( this ).next( '.collapsible-content' ).slideToggle( 'fast', function() {

			if ( $( this ).is( ':visible' ) ) {

				$( title ).addClass( 'open' );

			} else {

				$( title ).removeClass( 'open' );

			}

		});

	});

	$( '.collapsible-content' ).slideUp();

	// Collapsible sections
	function ddUpdate() {

		$( '.dd-text' ).each( function() {

			var $this = $( this );

			if ( $this.is( ':visible' ) ) {

				$this.siblings( '.dd-title' ).addClass( 'dd-open' );

			} else {

				$this.siblings( '.dd-title' ).removeClass( 'dd-open' );

			}

		});

	}

	function ddToggle( ele, hideall ) {

		var $this		= $( ele );
		var $thisText	= $this.siblings( '.dd-text' );

		if ( hideall ) {

			$( '.dd-text' ).not( $thisText ).stop().slideUp( 300, function() {

				ddUpdate();

			});

		}
		if ( $thisText.css( 'display' ) === 'none' ) {

			$thisText.stop().slideDown( 300, function() {

				ddUpdate();

			});

		} else {

			$thisText.stop().slideUp( 300, function() {

				ddUpdate();

			});

		}

	}

	$( '.dd-text' ).hide();
	ddUpdate();

	$( '.dd-title' ).on( 'click', function() {

		ddToggle( this );

	});

	// Full height image
	function setFullHeight() {

		$( '.block-full-height-image' ).each( function() {

			var offset = $( this ).offset();
			var top = offset.top;
			var windowHeight = $( window ).height();
			var height = windowHeight - top;

			$( this ).children( '.full-height-image' ).height( height );

		});

	}

	$( window ).on( 'resize', function() {

		setFullHeight();

	});

	setTimeout( function() {

		setFullHeight();

	}, 500 );

});
