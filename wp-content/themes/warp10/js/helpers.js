jQuery( function( $ ) {

	var $window		= $( window );
	var $stickyNav	= $( '.header-nav-sticky' ).find( '.header-nav' );
	var navPos		= 0;
	var navHeight	= 0;

	if ( $stickyNav.length > 0 ) {

		navPos		= $stickyNav.offset().top;
		navHeight	= $stickyNav.height();

	}

	/*
	* Smooth Scrolling
	*
	* Handles smooth scrolling for # links, only applies to a elements
	*/
	$( 'a[href^="#"]:not([href="#"])' ).on( 'click', function() {

		var target;
		var scrollDest;

		if ( location.pathname.replace( /^\//, '' ) === this.pathname.replace( /^\//, '' ) && location.hostname === this.hostname ) {

			target = $( this.hash );

			target = target.length ? target : $( '#' + this.hash.slice( 1 ) );
			if ( target.length ) {

				scrollDest = target.offset().top - navHeight;

				$( 'html,body' ).stop().animate({ 'scrollTop': scrollDest }, 1000 );

			}

		}

		return false;

	});

	/*
	* Stop '#' links making browser jump to the top of the page
	*/
	$( 'a[href="#"]' ).on( 'click', function( e ) {

		e.preventDefault();

	});


	/*
	* Custom placeholder for inputs
	* e.g. <input class="inline-val" default-val="Name*" placeholder="Name*">
	*/
	$( '.inline-val' ).on( 'focus', function() {

		var defaultVal = $( this ).data( 'default-value' );
		var val = $( this ).val();

		if ( defaultVal === val ) {

			$( this ).val( '' );

		}

	});

	$( '.inline-val' ).on( 'blur', function() {

		var defaultVal = $( this ).data( 'default-value' );
		var val = $( this ).val();

		if ( val === '' ) {

			$( this ).val( defaultVal );

		}

	});


	/*
	* Custom validation for forms
	*
	* Apply .has-validation class to the form tag
	* Apply .validate-this to inputs you want to validate
	* Create a div with id of myFormError to display any validation errors
	*/
	$( '.has-validation' ).on( 'submit', function( e ) {

		var validate = true;

		$( '.validate-this' ).each( function() {

			if ( $( this ).val() === $( this ).data( 'default-value' ) || $( this ).val() === '' ) {

				validate = false;

			}

		});
		if ( ! validate ) {

			$( '#myFormError' ).html( 'Please fill in all required fields.' ).show();
			e.preventDefault();

		}

	});


	/*
	* Sticky nav menu
	*/
	$window.on( 'scroll', function() {

		var y = $window.scrollTop();

		if ( y > navPos ) {

			$stickyNav.addClass( 'header-fixed' );

		} else {

			$stickyNav.removeClass( 'header-fixed' );

		}

	});


	/*
	* Cookie handling functions
	*/

	/**
	* Create a cookie
	*
	* name (str)
	* value (str)
	* days (int)
	*
	*/
	function createCookie( name, value, days ) {

		var date;
		var expires;

		if ( days ) {

			date = new Date();

			date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ) );
			expires = '; expires=' + date.toGMTString();

		} else {

			expires = '';

		}
		document.cookie = name + '=' + value + expires + '; path=/';

	}

	/**
	* Read a cookie
	*
	* name (str)
	*
	*/
	function readCookie( name ) {

		var i;
		var nameEQ = name + '=';
		var c;
		var ca = document.cookie.split( ';' );

		for ( i = 0; i < ca.length; i++ ) {

			c = ca[i];

			while ( c.charAt( 0 ) === ' ' ) {

				c = c.substring( 1, c.length );

			}
			if ( c.indexOf( nameEQ ) === 0 ) {

				return c.substring( nameEQ.length, c.length );

			}

		}

		return null;

	}

	/**
	* Erase a cookie
	*
	* name (str)
	*
	*/
	/* function eraseCookie( name ) {

		createCookie( name, '', -1 );

	}*/


	/*
	* Cookie msg
	*/
	$( '.cookie-bar-close' ).on( 'click', function() {

		createCookie( 'cookieAcceptance', 1, 999 );
		$( '.cookie-bar' ).fadeOut();

	});

	if ( ! readCookie( 'cookieAcceptance' ) ) {

		$( '.cookie-bar' ).show();

	}

});
