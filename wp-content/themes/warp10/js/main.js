/* global Modernizr */

jQuery( function( $ ) {

	var $window				= $( window );
	var	$pageSlide			= $( '.page-slide' );
	var	$mmInner			= $( '.mm-inner' );
	var	$sidebar			= $( '.sidebar' );
	var	$sidebarToggle		= $( '.sidebar-toggle' );
	var	$sidebarContent		= $( '.sidebar-content' );
	var smartresize;
	var smartscroll;


	/*
	* Mobile Menu
	*/
	$( '.mm-toggle' ).on( 'click', function() {

		if ( $pageSlide.attr( 'data-menu-state' ) === 'closed' ) {

			$pageSlide.attr( 'data-menu-state', 'open' );

		} else {

			$pageSlide.attr( 'data-menu-state', 'closed' );

		}

	});


	/*
	* Sidebar
	*/
	$sidebarToggle.on( 'click', function() {

		$sidebarContent.stop().slideToggle();
		if ( $sidebar.attr( 'data-mbl-state' ) === 'closed' ) {

			$sidebar.attr( 'data-mbl-state', 'open' );

		} else {

			$sidebar.attr( 'data-mbl-state', 'closed' );

		}

	});




	/*
	* Lazy load for images
	*
	* Target all images with .lazy class
	* <img src="" data-original="/path/image.png"> - Note, the SRC should be blank, using data-original to load in the image
	*/
	if ( $().lazyload ) {

		$( 'img.lazy' ).lazyload();

	}



	/*
	* Isotope
	*
	* Used to do grid layout and filtering on blog pages
	*/

	$( '.posts-filter-links' ).on( 'click', 'span', function() {

		var filterValue;

		$( '.active' ).removeClass( 'active' );
		$( this ).addClass( 'active' );
		filterValue = $( this ).attr( 'data-filter' );

		$( '.posts-grid' ).isotope({ 'filter': filterValue });

	});

	$window.on( 'load', function() {

		$( '.blog-posts .grid' ).isotope({
			'itemSelector': '.post-item',
			'masonry': { 'columnWidth': '.grid-sizer' },
		});

	});


	/*
	* Smart resize and scroll
	*/

	function debounce( func, wait, immediate ) {

		var timeout;


		return function() {

			var context = this;
			var args = arguments;
			var later = function() {

				timeout = null;
				if ( ! immediate ) {

					func.apply( context, args );

				}

			};
			var callNow = immediate && ! timeout;

			clearTimeout( timeout );
			timeout = setTimeout( later, wait );
			if ( callNow ) {

				func.apply( context, args );

			}

		};

	}

	jQuery.fn.smartresize = function( fn ) {

		return fn ? this.bind( 'resize', debounce( fn, 100 ) ) : this.trigger( smartresize );

	};

	jQuery.fn.smartscroll = function( fn ) {

		return fn ? this.bind( 'scroll', debounce( fn, 100 ) ) : this.trigger( smartscroll );

	};

	$window.smartresize( function() {

		if ( Modernizr.mq( '(min-width: 1025px)' ) ) {

			$pageSlide.attr( 'data-menu-state', 'closed' );
			$sidebarContent.removeAttr( 'style' );
			$sidebar.attr( 'data-mbl-state', 'closed' );

		}

	});

	$window.smartscroll( function() {

		if ( $mmInner.length > 0 ) {

			if ( $window.scrollTop() > $mmInner.offset().top + $mmInner.height() ) {

				$pageSlide.not( '.header-nav-sticky' ).attr( 'data-menu-state', 'closed' );

			}

		}

	});

});
