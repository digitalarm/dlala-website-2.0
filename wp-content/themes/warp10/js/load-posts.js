/* global w10LoadPosts */

jQuery( function( $ ) {

	// Number of next page to load
	var pageNum = parseInt( w10LoadPosts.startPage ) + 1;

	// Max number of pages current query can return
	var max = parseInt( w10LoadPosts.maxPages );

	// Link of next page of posts
	var nextLink = w10LoadPosts.nextLink;

	if ( pageNum > max ) {

		$( '.load-posts' ).remove();

	}

	// Load new posts when the link is clicked
	$( '.load-posts' ).on( 'click', function() {

		// Check there are posts to load
		if ( pageNum <= max ) {

			$( this ).text( 'LOADING...' );

			$.ajax({
				'url': nextLink,
				'success': function( data ) {

					var $items = $( data ).find( '.post-item' );

					// Append to Isotope layout
					$( '.blog-posts .grid' ).append( $items ).waitForImages( function() {

						$( this ).isotope( 'appended', $items );

					});

					// Update page number and next link
					pageNum++;
					nextLink = nextLink.replace( /\/page\/[0-9]*/g, '/page/' + pageNum );

					// Update the button
					if ( pageNum <= max ) {

						$( '.load-posts' ).html( '<span class="fa fa-plus-circle"></span> LOAD MORE POSTS' );

					} else {

						$( '.load-posts' ).html( 'NO MORE POSTS TO LOAD' );

					}

				},
			});

		} else {
			// $('#load-posts a').append('.');
		}

	});

});
