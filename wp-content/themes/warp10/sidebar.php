<aside class="sidebar" data-mbl-state="closed">
	<span class="sidebar-toggle"><?php _e( 'Sidebar Title', 'w10' ); ?></span>
	<div class="sidebar-content">
		<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'sidebar' ) ) : ?>
			<div class="pre-widget">
				<span class="sidebar-title"><?php _e( 'Widgetized Sidebar', 'w10' ); ?></span>
				<p><?php _e( 'This panel is active and ready for you to add some widgets via the WP Admin', 'w10' ); ?></p>
			</div>
		<?php endif; ?>
	</div>
</aside>
