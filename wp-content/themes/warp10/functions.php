<?php
define( 'SITE_VERSION', '1.0.2' );

/*
* Customizer
* 
* Wordpress Customizer configuration
*/
require_once( dirname( __FILE__ ) . '/incs/customizer.php' );


/*
* Defaults
* 
* WordPress defaults.
* Load default scripts.
* Load default styles.
*/
require_once( dirname( __FILE__ ) . '/incs/defaults.php' );


/*
* ACF
* 
* Custom fields of the advanced kind.
*/
require_once( dirname( __FILE__ ) . '/incs/acf-defaults.php' );


/*
* Helpers
* 
* Functions that do nice things.
*/
require_once( dirname( __FILE__ ) . '/incs/helpers.php' );


/*
* Navigation
* 
* Register menus for WP admin.
*/
require_once( dirname( __FILE__ ) . '/incs/nav.php' );


/*
* Posts
* 
* Register post types and post settings
*/
require_once( dirname( __FILE__ ) . '/incs/posts.php' );


/*
* Theme
* 
* Load styles and scripts and any custom bits for this theme
*/
require_once( dirname( __FILE__ ) . '/incs/theme.php' );
