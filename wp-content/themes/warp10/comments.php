<?php
/*
* If the current post is protected by a password and
* the visitor has not yet entered the password we will
* return early without loading the comments.
*/
if ( post_password_required() ) {
	return;
}
?>

<div class="comments-area">
	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php $comments_number = get_comments_number();
			if ( 1 === $comments_number ) {
				printf( _x( 'One thought on &ldquo;%s&rdquo;', 'comments title', 'w10' ), get_the_title() );
			} else {
				printf(
					_nx(
						'%1$s thought on &ldquo;%2$s&rdquo;',
						'%1$s thoughts on &ldquo;%2$s&rdquo;',
						$comments_number,
						'comments title',
						'w10'
					),
					number_format_i18n( $comments_number ),
					get_the_title()
				);
			} ?>
		</h2>

		<?php the_comments_navigation(); ?>

		<ul class="comment-list list-reset">
			<?php wp_list_comments(
					array(
					'short_ping'	=> TRUE,
					'avatar_size'	=> 48,
					'callback'		=> 'w10_comment',
				)
			); ?>
		</ul>

		<?php the_comments_navigation(); ?>
	<?php endif; ?>

	<?php if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'w10' ); ?></p>
	<?php endif; ?>

	<?php comment_form(
		array(
			'title_reply_before'	=> '<h2 class="comment-reply-title">',
			'title_reply_after' 	=> '</h2>',
			'class_submit'			=> 'btn',
		)
	); ?>
</div>
