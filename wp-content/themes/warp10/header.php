<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<title><?php wp_title(); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="<?php echo get_stylesheet_uri(); ?>" rel="stylesheet" type="text/css" media="screen">
		<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<?php if ( ! is_page_template( 'page-templates/page-splash.php' ) ) : ?>
			<div class="page-slide-container">

				<?php /*************************************************************

				Add header-nav-sticky class to page-slide for fixed header on scroll

				****************************************************************/ ?>

				<div class="page-slide" data-menu-state="closed">
					<?php if ( has_nav_menu( 'header-menu' ) ) : ?>
						<div class="mbl-menu">
							<div class="mm-inner">
								<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => 'nav' ) ); ?>
							</div>
						</div>
					<?php endif; ?>
					<header >
						<div class="container">
							<?php if ( function_exists( 'has_custom_logo' ) ) : ?>
								<?php if ( has_custom_logo() ) : ?>
									<?php the_custom_logo(); ?>
								<?php else: ?>
									<a class="logo" href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo( 'template_directory' ); ?>/dist/img/logo.png" alt="<?php bloginfo( 'name' ); ?>" title="<?php bloginfo( 'name' ); ?>"></a>
								<?php endif; ?>
							<?php else: ?>
								<a class="logo" href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo( 'template_directory' ); ?>/dist/img/logo.png" alt="<?php bloginfo( 'name' ); ?>" title="<?php bloginfo( 'name' ); ?>"></a>
							<?php endif; ?>
							<?php if( has_nav_menu( 'header-menu' ) ) : ?>
							<div class="header-nav">
								
									<div class="mm-toggle">
										<span class="mmt-bar mmt-top"></span>
										<span class="mmt-bar mmt-middle"></span>
										<span class="mmt-bar mmt-bottom"></span>
										<?php _e( 'Menu', 'w10' ); ?>
									</div>
									<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => 'nav' ) ); ?>
								
							</div>
						<?php endif; ?>
						</div>						
					</header>
		<?php endif; ?>
					<main>
