<div class="block block-video-slider spacing-<?php the_sub_field( 'spacing' ); ?>">
	<?php if ( get_sub_field( 'display' ) == 'large' ) : ?>
		<div class="container">
	<?php endif; ?>

		<div class="video-slider<?php if ( get_sub_field( 'thumbnail_navigation' ) == 'Yes' ) : ?>-with-nav<?php endif; ?>">
			<div class="video-slides">
				<?php while ( have_rows( 'slides' ) ) : the_row();
					$image = get_sub_field( 'cover_image' )  ?>
					<div class="slick-slide"<?php if ( $image ) : ?> style="background-image: url(<?php echo $image['sizes']['hero']; ?>);"<?php endif; ?>>
						<div class="video-slide-cover-text">
							<?php the_sub_field( 'cover_text_overlay' ); ?>
							<span class="video-slide-play"><?php _e( 'Play', 'w10' ); ?></span>
						</div>
						<div class="video-container"<?php if ( $image ) : ?> style="visibility:hidden;"<?php endif; ?>><?php the_sub_field( 'video_url' ); ?></div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
		
	<?php if ( get_sub_field( 'display' ) == 'large' ) : ?>
		</div>
	<?php endif; ?>

	<?php if ( get_sub_field( 'thumbnail_navigation' ) == 'Yes' ) : ?>
		<?php if ( get_sub_field( 'display' ) == 'large' ) : ?>
			<div class="container">
		<?php endif; ?>
			<div class="video-slider-nav">
				<div class="nav-slides">
					<?php while ( have_rows( 'slides' ) ) : the_row();
						$image = get_sub_field( 'cover_image' ); ?>
						<div class="slick-slide">
							<img src="<?php echo $image['sizes']['hero']; ?>" alt="<?php echo $image['alt']; ?>">
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		<?php if ( get_sub_field( 'display' ) == 'large' ) : ?>
			</div>
		<?php endif; ?>
	<?php endif; ?>
</div>
