<div class="block block-google-map spacing-<?php the_sub_field( 'spacing' ); ?>">
	<div class="acf-map">
		<?php if ( have_rows( 'markers' ) ) : ?>
			<?php while ( have_rows( 'markers' ) ) : the_row(); ?>
				<?php $location = get_sub_field( 'marker' );
				if( ! empty( $location ) ) : ?>
					<?php if ( get_sub_field( 'use_tooltip' ) == 'Yes' ) : ?>
						<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>" data-lab="<?php echo $location['address']; ?>">
							<?php if ( have_rows( 'tooltip' ) ) : ?>
								<?php while ( have_rows( 'tooltip' ) ) : the_row(); ?>
									<?php echo the_sub_field( 'content' ); ?>
									<?php if ( get_sub_field( 'show_directions_button' ) == 'Yes' ) : ?>
										<form class="map-directions" action="https://maps.google.com/maps" method="get" target="_blank">
											<label for="saddr"><?php _e( 'Get directions from...', 'w10' ); ?></label>
											<input type="submit" class="map-directions-submit" value="GO">
											<div class="map-directions-input">
												<input type="text" name="saddr" id="saddr" placeholder="Enter your postcode">
												<input type="hidden" name="daddr" value="<?php echo $location['address']; ?>">
											</div>
										</form>
									<?php endif; ?>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					<?php else: ?>
						<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>" data-lab="<?php echo $location['address']; ?>"></div>
					<?php endif; ?>
				<?php endif; ?>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>
