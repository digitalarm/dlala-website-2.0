<?php
$background_image = '';
$background_style = '';
$background_colour = '';
$custom_background_colour = '';

if ( have_rows( 'background' ) ) {
	while ( have_rows( 'background' ) ) {
		the_row();
		if ( get_sub_field( 'use_background_image' ) ) {
			if ( have_rows( 'background_image' ) ) {
				while ( have_rows( 'background_image' ) ) {
					the_row();
					$background_image = get_sub_field( 'image' );
					$background_style = get_sub_field( 'style' );
				}
			}
		}
		$background_colour = get_sub_field( 'background_colour' );
		if ( $background_colour == 'custom' ) {
			$custom_background_colour = get_sub_field( 'custom_background_colour' );
		}
	}
}
$images = get_sub_field( 'images' );
?>
<div class="block block-gallery<?php if ( $background_style ) { echo ' bg-' . $background_style; }; if ( $background_colour != 'custom' && $background_colour != 'none' ) { echo ' bg-' . $background_colour; } ?> spacing-<?php the_sub_field( 'spacing' ); ?>" style="<?php if ( $background_image ) { echo 'background-image: url(' . $background_image['url'] . ');'; } if ( $custom_background_colour ) { echo ' background-color: ' . $custom_background_colour . ';'; } ?>">
	<?php if ( get_sub_field( 'display' ) == 'grid' ) : ?>
		<div class="container">
			<div class="grid">
				<?php foreach ( $images as $image ) : ?>
					<div class="gallery-item">
						<a class="gi-inner" href="<?php echo $image['url']; ?>" target="_blank">
							<img src="<?php echo $image['sizes']['gallery-thumbnail']; ?>" title="<?php echo $image['title']; ?>" alt="<?php echo $image['alt']; ?>">
							<?php if ($image['caption']) : ?>lo
								<p class="wp-caption-text"><?php echo $image['caption']; ?></p>
							<?php endif; ?>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php elseif ( get_sub_field( 'display' ) == 'carousel' ) : ?>
		<div class="carousel-slider">
			<div class="carousel-slides">
				<?php foreach ( $images as $image ) : ?>
					<div class="carousel-slide">
						<img src="<?php echo $image['sizes']['gallery-thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>">
						<?php if ( $image['caption'] ) : ?>
							<p class="wp-caption-text"><?php echo $image['caption']; ?></p>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php else: ?>
		<div class="container">
			<div class="slideshow-slider">
				<div class="slideshow-slides">
					<?php foreach ( $images as $image ) : ?>
						<div class="slideshow-slide">
							<img src="<?php echo $image['sizes']['gallery']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>">
							<?php if ( $image['caption'] ) : ?>
								<p class="wp-caption-text"><?php echo $image['caption']; ?></p>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
