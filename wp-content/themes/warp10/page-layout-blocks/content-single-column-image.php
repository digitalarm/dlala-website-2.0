<?php $image = get_sub_field( 'image' ); ?>
<div class="block block-single-column-image spacing-<?php the_sub_field( 'spacing' ); ?>">
	<?php if ( get_sub_field( 'display' ) == 'large' || get_sub_field( 'display' ) == 'small' ) : ?>
		<div class="container<?php if ( get_sub_field( 'display' ) == 'small' ) : ?> half<?php endif; ?>">
	<?php endif; ?>
		<?php if ( get_sub_field( 'link' ) ) : ?>
			<a href="<?php the_sub_field( 'link' ); ?>">
		<?php endif; ?>
			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>">
		<?php if ( get_sub_field( 'link' ) ) : ?>
			</a>
		<?php endif; ?>
	<?php if ( get_sub_field( 'display' ) == 'large' || get_sub_field( 'display' ) == 'small' ) : ?>
		</div>
	<?php endif; ?>
</div>
