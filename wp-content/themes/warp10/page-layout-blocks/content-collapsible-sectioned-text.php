<?php
$background_image = '';
$background_style = '';
$background_colour = '';
$custom_background_colour = '';

if ( have_rows( 'background' ) ) {
	while ( have_rows( 'background' ) ) {
		the_row();
		if ( get_sub_field( 'use_background_image' ) ) {
			if ( have_rows( 'background_image' ) ) {
				while ( have_rows( 'background_image' ) ) {
					the_row();
					$background_image = get_sub_field( 'image' );
					$background_style = get_sub_field( 'style' );
				}
			}
		}
		$background_colour = get_sub_field( 'background_colour' );
		if ( $background_colour == 'custom' ) {
			$custom_background_colour = get_sub_field( 'custom_background_colour' );
		}
	}
}
?>

<div class="block block-collapsible-sectioned-text<?php if ( $background_style ) { echo ' bg-' . $background_style; }; if ( $background_colour != 'custom' && $background_colour != 'none' ) { echo ' bg-' . $background_colour; } ?> spacing-<?php the_sub_field( 'spacing' ); ?>" style="<?php if ( $background_image ) { echo 'background-image: url(' . $background_image['url'] . ');'; } if ( $custom_background_colour ) { echo ' background-color: ' . $custom_background_colour . ';'; } ?>">
	<div class="container">
		<?php the_sub_field( 'intro' ); ?>
		<?php if( have_rows( 'sections' ) ) : ?>
			<ul class="dd-list list-reset">
				<?php while ( have_rows( 'sections' ) ) : the_row();
					$background_colour = '';
					$custom_background_colour = '';
					$colour = '';
					$custom_colour = '';
					$background_colour = get_sub_field( 'background_colour' );
					if ( $background_colour == 'custom' ) :
						$custom_background_colour = get_sub_field( 'custom_background_colour' );
					endif;
					$colour = get_sub_field( 'colour' );
					if ( $colour == 'custom' ) :
						$custom_colour = get_sub_field( 'custom_colour' );
					endif; ?>
					<li class="dd-block<?php if ( $background_colour != 'custom' && $background_colour != 'none' ) : echo ' bg-' . $background_colour; endif; if ( $colour != 'custom' ) : echo ' ' . $colour; endif; ?>" style="<?php if ( $custom_background_colour ) : echo ' background-color: ' . $custom_background_colour . ';'; endif; if ( $custom_colour ) : echo ' color: ' . $custom_colour . ';'; endif; ?>">
						<div class="dd-title">
							<span class="dd-title-inner"><?php the_sub_field( 'title' ); ?></span>
							<span class="plus icon"><?php _e( 'Open', 'w10' ); ?></span>
							<span class="minus icon"><?php _e( 'Close', 'w10' ); ?></span>
						</div>
						<div class="dd-text">
							<?php the_sub_field( 'text' ); ?>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>
