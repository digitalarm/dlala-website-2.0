<?php
$background_image = '';
$background_style = '';
$background_colour = '';
$custom_background_colour = '';

if ( have_rows( 'background' ) ) {
	while ( have_rows( 'background' ) ) {
		the_row();
		if ( get_sub_field( 'use_background_image' ) ) {
			if ( have_rows( 'background_image' ) ) {
				while ( have_rows( 'background_image' ) ) {
					the_row();
					$background_image = get_sub_field( 'image' );
					$background_style = get_sub_field( 'style' );
				}
			}
		}
		$background_colour = get_sub_field( 'background_colour' );
		if ( $background_colour == 'custom' ) {
			$custom_background_colour = get_sub_field( 'custom_background_colour' );
		}
	}
}
?>

<div class="block block-latest-posts<?php if ( $background_style ) { echo ' bg-' . $background_style; }; if ( $background_colour != 'custom' && $background_colour != 'none' ) { echo ' bg-' . $background_colour; } ?> spacing-<?php the_sub_field( 'spacing' ); ?>" style="<?php if ( $background_image ) { echo 'background-image: url(' . $background_image['url'] . ');'; } if ( $custom_background_colour ) { echo ' background-color: ' . $custom_background_colour . ';'; } ?>">
	<div class="container">
		<div class="grid">
			<?php $limit = get_sub_field( 'display' );
			$width = floor( 100 / $limit );

			$args = array(
				'post_type' => 'post',
				'posts_per_page' => $limit,
				'orderby' => 'date',
				'order' => 'DESC'
			);
			$posts = get_posts( $args );

			foreach( $posts as $post ) : ?>
				<div class="column-<?php echo $width; ?>">
					<?php // get_the_post_thumbnail( $post->ID, 'medium' ); ?>
					<span class="title"><?php echo $post->post_title; ?></span>
					<div class="latest-post-excerpt"><?php echo $post->post_excerpt; ?></div>
					<a href="<?php echo get_permalink( $post->ID ); ?>"><?php _e( 'Read post', 'w10' ); ?></a>
				</div>	
			<?php endforeach; ?>
		</div>
	</div>
</div>
