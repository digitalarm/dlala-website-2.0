<?php 
	$image = get_sub_field( 'image' ); 
	$next_section = time();
?>
<div class="block block-full-height-image">
	<div class="full-height-image" style="background-image:url('<?php echo $image['url']; ?>');">
		<?php if ( get_sub_field( 'text_overlay' ) ) : ?>
			<div class="full-height-image-text-overlay">
				<?php the_sub_field( 'text_overlay' ); ?>
			</div>
		<?php endif; ?>
	</div>
	<a class="full-height-next-section" href="#<?php echo $next_section; ?>"><span class="fa fa-angle-down"></span></a>
</div>
<span id="<?php echo $next_section; ?>"></span>
