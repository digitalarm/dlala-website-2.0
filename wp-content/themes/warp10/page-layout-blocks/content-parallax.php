<?php $image = get_sub_field( 'image' ); ?>
<div class="block block-parallax spacing-<?php the_sub_field( 'spacing' ); ?>">
	<div class="parallax-image" style="background-image:url('<?php echo $image['url']; ?>');">
		<div class="parallax-image-text-overlay"><?php the_sub_field( 'text_overlay' ); ?></div>
	</div>
</div>
