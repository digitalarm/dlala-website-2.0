<div class="block block-page-links">
	<?php while ( has_sub_fields ( 'link_panels' ) ) : ?>
		<div class="link-panel">
			<div class="link-panel-image">
				<?php $bg = get_sub_field ( 'background_image' ); ?>
				<img src="<?php echo $bg['sizes']['link-panel']; ?>" alt="" >
			</div>
			<a href="<?php if ( the_sub_field ( 'link' ) ) : the_sub_field ( 'link' ) ; else: the_sub_field( 'anchor_link' ) ; endif; ?>" class="link-panel-overlay">
				<span class="link-panel-overlay-inner wow zoomInUp" data-wow-delay=".1s">
					<span class="link-panel-icon">
						<img src="<?php $icon = get_sub_field ( 'icon' ); echo $icon['url']; ?>" alt="<?php the_sub_field ( 'title' ); ?>" />
					</span>
					<span class="link-panel-text"><?php the_sub_field( 'title' ); ?></span>	
				</span>			
			</a>
		</div>
	<?php endwhile; ?>
	<div class="clear"></div>
</div>
