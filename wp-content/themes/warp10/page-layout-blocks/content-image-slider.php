<div class="block block-image-slider spacing-inside bg-grey">
	<?php if ( get_sub_field( 'display' ) == 'large' ) : ?>
		<div class="container">
	<?php endif; ?>
		<div class="image-slider<?php if ( get_sub_field( 'thumbnail_navigation' ) == 'Yes' ) : ?>-with-nav<?php endif; ?>">
			<div class="image-slides">
			<?php while ( have_rows( 'slides' ) ) : the_row();
				$image = get_sub_field( 'image' ); ?>
				<div class="slick-slide">
					<?php if ( get_sub_field( 'link' ) ) : ?><a href="<?php the_sub_field( 'link' ); ?>"><?php endif; ?>
						<img src="<?php echo $image['sizes']['hero']; ?>" alt="<?php echo $image['alt']; ?>">
					<?php if ( get_sub_field( 'link' ) ) : ?></a><?php endif; ?>
				</div>
			<?php endwhile; ?>
			</div>
		</div>
	<?php if ( get_sub_field( 'display' ) == 'large' ) : ?>
		</div>
	<?php endif; ?>
	<?php if ( get_sub_field( 'thumbnail_navigation' ) == 'Yes' ) : ?>
		<?php if ( get_sub_field( 'display' ) == 'large' ) : ?>
			<div class="container">
		<?php endif; ?>
			<div class="image-slider-nav">
				<div class="nav-slides">
					<?php while ( have_rows( 'slides' ) ) : the_row();
						$image = get_sub_field( 'image' ); ?>
						<div class="slick-slide">
							<img src="<?php echo $image['sizes']['hero']; ?>" alt="<?php echo $image['alt']; ?>">
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		<?php if ( get_sub_field( 'display' ) == 'large' ) : ?>
			</div>
		<?php endif; ?>
	<?php endif; ?>
</div>
