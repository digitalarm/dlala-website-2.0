<?php 
if ( have_rows( 'layout_blocks' ) ) :
	while ( have_rows( 'layout_blocks' ) ) : the_row();
	
		if ( get_row_layout() == 'single_column_text' ) :

			get_template_part( 'page-layout-blocks/content', 'single-column-text' );

		elseif ( get_row_layout() == 'single_column_image' ) :

			get_template_part( 'page-layout-blocks/content', 'single-column-image' );

		elseif ( get_row_layout() == 'single_column_video' ) :

			get_template_part( 'page-layout-blocks/content', 'single-column-video' );

		elseif ( get_row_layout() == 'two_column_text' ) :

			get_template_part( 'page-layout-blocks/content', 'two-column-text' );

		elseif ( get_row_layout() == 'two_column_text_image' ) :

			get_template_part( 'page-layout-blocks/content', 'two-column-text-image' );

		elseif ( get_row_layout() == 'three_column_text' ) :

			get_template_part( 'page-layout-blocks/content', 'three-column-text' );

		elseif ( get_row_layout() == 'collapsible_sectioned_text' ) :

			get_template_part( 'page-layout-blocks/content', 'collapsible-sectioned-text' );

		elseif ( get_row_layout() == 'text_slider' ) :

			get_template_part( 'page-layout-blocks/content', 'text-slider' );

		elseif ( get_row_layout() == 'image_slider' ) :

			get_template_part( 'page-layout-blocks/content', 'image-slider' );

		elseif ( get_row_layout() == 'video_slider' ) :

			get_template_part( 'page-layout-blocks/content', 'video-slider' );

		elseif ( get_row_layout() == 'gallery' ) :

			get_template_part( 'page-layout-blocks/content', 'gallery' );

		elseif ( get_row_layout() == 'parallax' ) :

			get_template_part( 'page-layout-blocks/content', 'parallax' );

		elseif ( get_row_layout() == 'latest_posts' ) :

			get_template_part( 'page-layout-blocks/content', 'latest-posts' );

		elseif ( get_row_layout() == 'google_map' ) :

			get_template_part( 'page-layout-blocks/content', 'google-map' );

		elseif ( get_row_layout() == 'collapsible_content' ) :

			get_template_part( 'page-layout-blocks/content', 'collapsible' );

		elseif ( get_row_layout() == 'full_height_image' ) :

			get_template_part( 'page-layout-blocks/content', 'full-height-image' );

		elseif ( get_row_layout() == 'image_video_slider' ) :

			get_template_part( 'page-layout-blocks/content', 'image-video-slider' );

		elseif ( get_row_layout() == 'page_links' ) :

			get_template_part( 'page-layout-blocks/content', 'page-links' );

		// You can add additional layout blocks here.
		// e.g. elseif( get_row_layout() == 'my_layout_block' ) :
		// Then add your layout block into page-layout-blocks

		endif;

	endwhile;
endif;
