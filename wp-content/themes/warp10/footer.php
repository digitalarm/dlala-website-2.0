					</main>
		<?php if ( ! is_page_template( 'page-templates/page-splash.php' ) ) : ?>
					<footer>
						<div class="footer-logo"><?php the_custom_logo(); ?></div>
						<?php if ( has_nav_menu( 'footer-menu' ) ) : ?>
							<div class="footer-nav-bar">
								<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container_id' => 'footer-nav', 'container' => 'nav', 'container_class' => '' ) ); ?>
							</div>
						<?php endif; ?>
						<p class="footer-copy">
							<?php echo str_replace( 'DATE', date( 'Y' ), get_theme_mod( 'w10_copyright_text', '&copy; Company Name DATE' ) ); ?>
						</p>
						<p  class="footer-copy">
						<a href="https://www.digitalarm.co.uk" target="_blank">Website Design and Build</a> by <a href="https://www.digitalarm.co.uk" target="_blank">Digital Arm</a>
						</p>
					</footer>
				</div>
			</div>
		<?php endif; ?>
		<div class="cookie-bar"><?php _e( "If you use our website we'll assume you’re happy for us to use cookies.", 'w10'); ?><span class="cookie-bar-close btn">X</span></div>
		<?php if ( TRUE === get_theme_mod( 'gmaps_enabled', FALSE ) ) : ?>
			<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key="></script>
		<?php endif; ?>
		<?php wp_footer(); ?>
	</body>
</html>
