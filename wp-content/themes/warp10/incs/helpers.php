<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/*
* filter_post_vars
* Check a post variable for a link (useful for spam checking)
*/
if ( ! function_exists( 'filter_post_vars' ) ) {
	function filter_post_vars( $post, $exceptions = array() ) {
		$filter_list = array( '<a','href','[url','http://' );
		foreach ( $post as $filter_var => $filter_value ) {
			if ( ! in_array( $filter_var, $exceptions ) ) {
				foreach ( $filter_list as $filter_word ) {
					if ( FALSE !== strpos( $filter_value, $filter_word ) ) {
						return FALSE;
					}
				}
			}
		}
		return TRUE;
	}
}


/*
* time_ago
* Convert a MySQL datetime to seconds/minutes/days/weeks/months/years ago
*/
if ( ! function_exists( 'time_ago' ) ) {
	function time_ago( $mysql_date ) {
		$time = strtotime( $mysql_date );
		$time = time() - $time; // to get the time since that moment

		$tokens = array (
			31536000 => 'year',
			2592000 => 'month',
			604800 => 'week',
			86400 => 'day',
			3600 => 'hour',
			60 => 'minute',
			1 => 'second'
		);

		$time = abs( $time );
		foreach ( $tokens as $unit => $text ) {
			if ( $time < $unit ) {
				continue;
			}
			$numberOfUnits = floor( $time / $unit );
			return $numberOfUnits . ' ' . $text . ( ( $numberOfUnits > 1 ) ? 's' : '' );
		}			
	}
}


/* Custom search to include ACF fields */

/**
* [list_searcheable_acf list all the custom fields we want to include in our search query]
* @return [array] [list of custom fields]
*/
function list_searcheable_acf() {
	$list_searcheable_acf = array( "title", "content", "quote" );
	return $list_searcheable_acf;
}

/**
* [advanced_custom_search search that encompasses ACF/advanced custom fields and taxonomies and split expression before request]
* @param  [query-part/string]      $where    [the initial "where" part of the search query]
* @param  [object]                 $wp_query []
* @return [query-part/string]      $where    [the "where" part of the search query as we customized]
* see https://vzurczak.wordpress.com/2013/06/15/extend-the-default-wordpress-search/
* credits to Vincent Zurczak for the base query structure/splitting tags section
*/
function advanced_custom_search( $where, &$wp_query ) {
	global $wpdb;

	if ( empty( $where ) ) {
		return $where;
	}

	// get search expression
	$terms = $wp_query->query_vars['s'];

	// explode search expression to get search terms
	$exploded = explode( ' ', $terms );
	if ( FALSE === $exploded || count( $exploded ) == 0 ) {
		$exploded = array( 0 => $terms );
	}

	// reset search in order to rebuilt it as we whish
	$where = '';

	// get searcheable_acf, a list of advanced custom fields you want to search content in
	$list_searcheable_acf = list_searcheable_acf();
	foreach( $exploded as $tag ) {
		$where .= " AND ( (" . $wpdb->prefix . "posts.post_title LIKE '%$tag%') OR (" . $wpdb->prefix . "posts.post_content LIKE '%$tag%') OR EXISTS (SELECT * FROM " . $wpdb->prefix . "postmeta	WHERE post_id = " . $wpdb->prefix . "posts.ID	AND ( ";
		foreach ( $list_searcheable_acf as $searcheable_acf ) {
			if ( $searcheable_acf == $list_searcheable_acf[ 0 ] ) {
				$where .= " (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE '%$tag%') ";
			} else {
				$where .= " OR (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE '%$tag%') ";
			}
		}
		$where .= ")) OR EXISTS (SELECT * FROM " . $wpdb->prefix . "comments WHERE comment_post_ID = " . $wpdb->prefix . "posts.ID AND comment_content LIKE '%$tag%') OR EXISTS (SELECT * FROM " . $wpdb->prefix . "terms INNER JOIN " . $wpdb->prefix . "term_taxonomy	ON " . $wpdb->prefix . "term_taxonomy.term_id = " . $wpdb->prefix . "terms.term_id INNER JOIN " . $wpdb->prefix . "term_relationships	ON " . $wpdb->prefix . "term_relationships.term_taxonomy_id = " . $wpdb->prefix . "term_taxonomy.term_taxonomy_id WHERE (taxonomy = 'post_tag' OR taxonomy = 'category'	OR taxonomy = 'myCustomTax') AND object_id = " . $wpdb->prefix . "posts.ID AND " . $wpdb->prefix . "terms.name LIKE '%$tag%'))";
	}
	return $where;
}

add_filter( 'posts_search', 'advanced_custom_search', 500, 2 );
