<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Define constants
define( 'LIB_PATH', get_bloginfo('template_directory') . '/lib' );


// Remove WordPress admin bar
add_filter( 'show_admin_bar', '__return_false' );


// Clean up head
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


// Load scripts
function w10_load_lib_scripts() {

	if ( TRUE === get_theme_mod( 'waitforimages_enabled', TRUE ) ) {
		wp_enqueue_script( 'waitforimages', LIB_PATH . '/jquery/jquery.waitforimages.js', array('jquery'), '2.1.0', TRUE );
	}

	if ( TRUE === get_theme_mod( 'modernizr_enabled', TRUE ) ) {
		wp_enqueue_script( 'modernizr-js', LIB_PATH . '/modernizr/modernizr.min.js', '', '3.3.1' );
	}

	if ( TRUE === get_theme_mod( 'slick_enabled', TRUE ) ) {
		wp_enqueue_script( 'slick-js', LIB_PATH . '/slick/slick.min.js', array('jquery'), '1.6.0', TRUE );
	}

	if ( TRUE === get_theme_mod( 'magnificpopup_enabled', TRUE ) ) {
		wp_enqueue_script( 'magnificpopup-js', LIB_PATH . '/magnificpopup/magnific-popup.js', array('jquery'), '1.1.0', TRUE );
	}

	if ( TRUE === get_theme_mod( 'hoverizr_enabled', FALSE ) ) {
		wp_enqueue_script( 'hoverizr-js', LIB_PATH . '/jquery/jquery.hoverizr.min.js', array('jquery'), '1.0', TRUE );
	}

	if ( TRUE === get_theme_mod( 'wow_enabled', FALSE ) ) {
		wp_enqueue_script( 'wow-js', LIB_PATH . '/wow/wow.min.js', '', '1.0.2', TRUE );
	}

	if ( TRUE === get_theme_mod( 'isotope_enabled', TRUE ) ) {
		wp_enqueue_script( 'isotope-js', LIB_PATH . '/isotope/isotope.pkgd.min.js', '', '3.0.0', TRUE );
	}

	if ( TRUE === get_theme_mod( 'lazyload_enabled', FALSE ) ) {
		wp_enqueue_script( 'lazyload-js', LIB_PATH . '/jquery/jquery.lazyload.min.js', array('jquery'), '1.9.7', TRUE );
	}

	if ( TRUE === get_theme_mod( 'gmaps_enabled', FALSE ) ) {
		wp_enqueue_script( 'maps-js', get_stylesheet_directory_uri() . '/js/maps.js', '', '0.0.1', TRUE );
	}
}
add_action( 'wp_enqueue_scripts', 'w10_load_lib_scripts' );


// Load styles
function w10_load_lib_styles() {

	if ( TRUE === get_theme_mod( 'normalize_enabled', TRUE ) ) {
		wp_enqueue_style( 'normalize', LIB_PATH . '/normalize/normalize.css', '', '4.1.1' );
	}

	if ( TRUE === get_theme_mod( 'slick_enabled', TRUE ) ) {
		wp_enqueue_style( 'slick-css', LIB_PATH . '/slick/slick.css', '', '1.6.0' );
	}

	if ( TRUE === get_theme_mod( 'magnificpopup_enabled', TRUE ) ) {
		wp_enqueue_style( 'magnificpopup-css', LIB_PATH . '/magnificpopup/magnific-popup.css', '', '1.1.0' );
	}

	if ( TRUE === get_theme_mod( 'fontawesome_enabled', TRUE ) ) {
		wp_enqueue_style( 'fontawesome-css', LIB_PATH . '/fontawesome/css/font-awesome.css', '', '4.6.3' );
	}

	if ( TRUE === get_theme_mod( 'animate_enabled', FALSE ) ) {
		wp_enqueue_style( 'animate-css', LIB_PATH . '/animate/animate.css', '', '3.5.1' );
	}
}
add_action( 'wp_enqueue_scripts', 'w10_load_lib_styles', 5 );


// Load admin styles
function w10_load_admin_styles() {
	$screen = get_current_screen();
	if ( $screen->post_type == 'page' ) {
		// If you need to include custom CSS for a certain page/post type.
	}

	wp_enqueue_style( 'admin-page', get_bloginfo('template_directory') . '/admin.css', '', SITE_VERSION );	
}
add_action( 'admin_enqueue_scripts', 'w10_load_admin_styles' );


// Disable comments (currently on all post types), also removes any approved comments
function w10_disable_comments() {
	global $wpdb;
	if ( $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE `comment_status` != 'closed' ") > 0 ) {
		$wpdb->query( "UPDATE $wpdb->posts SET `comment_status` = 'closed'" );
		$wpdb->query( "UPDATE $wpdb->comments SET `comment_approved` = '0'" );
	}
}

if ( FALSE === get_theme_mod( 'enable_comments', TRUE ) ) {
	add_action( 'admin_init', 'w10_disable_comments' );
}


// Disable trackbacks (currently on all post types)
function w10_disable_pingbacks() {
	global $wpdb;
	if ( $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE `ping_status` != 'closed' " ) > 0 ) {
		$wpdb->query( "UPDATE $wpdb->posts SET `ping_status` = 'closed'" );
	}
}

if ( FALSE === get_theme_mod( 'enable_pingbacks', FALSE ) ) {
	add_action( 'admin_init', 'w10_disable_pingbacks' );
}


// Set default blog media link to none
function w10_imagelink_setup() {
    $image_set = get_option( 'image_default_link_type' );
    
    if ($image_set !== 'none') {
        update_option( 'image_default_link_type', 'none' );
    }
}
add_action( 'admin_init', 'w10_imagelink_setup', 10 );


// Set Google Maps API key
function my_acf_init() {
	acf_update_setting( 'google_api_key', '' );
}
add_action( 'acf/init', 'my_acf_init' );


// Enqueue comment reply script
function enable_threaded_comments() {
if ( ( ! is_admin() ) && is_singular() && comments_open() && get_option( 'thread_comments' ) === 1 )
	wp_enqueue_script( 'comment-reply' );
}
add_action( 'wp_print_scripts', 'enable_threaded_comments' );
