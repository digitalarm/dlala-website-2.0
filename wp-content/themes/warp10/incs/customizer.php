<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
* Register Theme Customizer
*
* @param object $wp_customize An instance of the WP_Customize_Manager class
*
* @return Customizer settings and controls
*/
function w10_register_theme_customizer( $wp_customize ) {
	$wp_customize->add_section(
		'w10_post_settings',
		array(
			'title'		=> __( 'Single Post Settings', 'w10' ),
			'priority'	=> 200
		)
	);

	$wp_customize->add_setting(
		'w10_author_name',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'w10_author_name',
		array(
			'section'		=> 'w10_post_settings',
			'label'			=> __( 'Show Author Name', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'w10_author_image',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'w10_author_image',
		array(
			'section'		=> 'w10_post_settings',
			'label'			=> __( 'Show Author Avatar', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_section(
		'w10_template_fields',
		array(
			'title'		=> __( 'Template Fields', 'w10' ),
			'priority'	=> 201
		)
	);

	$wp_customize->add_setting(
		'w10_copyright_text',
		array(
			'default'	=> '&copy; Company Name DATE'
		)
	);

	$wp_customize->add_control(
		'w10_copyright_text',
		array(
			'section'		=> 'w10_template_fields',
			'label'			=> __( 'Footer Copyright Text', 'w10' ),
			'description'	=> '\'DATE\' ' . __( 'will be replaced with the current year', 'w10' ),
			'type'			=> 'text'
		)
	);

	$wp_customize->add_section(
		'w10_custom_javascript',
		array(
			'title'		=> __( 'Custom Javascript', 'w10' ),
			'priority'	=> 202
		)
	);

	$wp_customize->add_setting(
		'w10_header_javascript',
		array(
			'default'	=> ''
		)
	);

	$wp_customize->add_control(
		'w10_header_javascript',
		array(
			'section'	=> 'w10_custom_javascript',
			'label'		=> __( 'Header Javascript', 'w10' ),
			'type'		=> 'textarea'
		)
	);

	$wp_customize->add_setting(
		'w10_footer_javascript',
		array(
			'default'	=> ''
		)
	);

	$wp_customize->add_control(
		'w10_footer_javascript',
		array(
			'section'	=> 'w10_custom_javascript',
			'label'		=> __( 'Footer Javascript', 'w10' ),
			'type'		=> 'textarea'
		)
	);

	$wp_customize->add_panel(
		'w10_site_settings',
		array(
			'title'		=> __( 'Site Settings', 'w10' ),
			'priority'	=> 203
		)
	);

	$wp_customize->add_section(
		'w10_libraries',
		array(
			'panel'		=> 'w10_site_settings',
			'title'		=> __( 'Libraries', 'w10' ),
			'priority'	=> 1
		)
	);

	$wp_customize->add_setting(
		'normalize_enabled',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'normalize_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Normalize Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'modernizr_enabled',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'modernizr_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Modernizr Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'waitforimages_enabled',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'waitforimages_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Wait For Images Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'isotope_enabled',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'isotope_enabled',
		array(
			'section'		=> 'w10_libraries',
			'label'			=> __( 'Isotope Enabled', 'w10' ),
			'description'	=> __( '(Requires waitForImages)', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'slick_enabled',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'slick_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Slick Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'magnificpopup_enabled',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'magnificpopup_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Magnific Popup Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'hoverizr_enabled',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'hoverizr_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Hoverizr Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'fontawesome_enabled',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'fontawesome_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Font Awesome Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'wow_enabled',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'wow_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Wow JS Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'animate_enabled',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'animate_enabled',
		array(
			'section'	=> 'w10_libraries',
			'label'		=> __( 'Animate CSS Enabled', 'w10' ),
			'type'		=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'lazyload_enabled',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'lazyload_enabled',
		array(
			'section'		=> 'w10_libraries',
			'label'			=> __( 'Lazyload Enabled', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'gmaps_enabled',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'gmaps_enabled',
		array(
			'section'		=> 'w10_libraries',
			'label'			=> __( 'Google Maps Enabled', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_section(
		'w10_comments',
		array(
			'panel'		=> 'w10_site_settings',
			'title'		=> __( 'Comments', 'w10' ),
			'priority'	=> 2
		)
	);

	$wp_customize->add_setting(
		'enable_comments',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'enable_comments',
		array(
			'section'		=> 'w10_comments',
			'label'			=> __( 'Enable Comments', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'enable_pingbacks',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'enable_pingbacks',
		array(
			'section'		=> 'w10_comments',
			'label'			=> __( 'Enable Pingbacks', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_section(
		'w10_modules',
		array(
			'panel'		=> 'w10_site_settings',
			'title'		=> __( 'Modules', 'w10' ),
			'priority'	=> 3
		)
	);

	$wp_customize->add_setting(
		'mockups',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'mockups',
		array(
			'section'		=> 'w10_modules',
			'label'			=> __( 'Mockups', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_section(
		'w10_sidebar',
		array(
			'panel'		=> 'w10_site_settings',
			'title'		=> __( 'Sidebar', 'w10' ),
			'priority'	=> 3
		)
	);

	$wp_customize->add_setting(
		'fixed_sidebar',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'fixed_sidebar',
		array(
			'section'		=> 'w10_sidebar',
			'label'			=> __( 'Fixed Sidebar', 'w10' ),
			'description'	=> __( 'Sidebar will follow the window when scrolling', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_section(
		'w10_sitemap',
		array(
			'panel'		=> 'w10_site_settings',
			'title'		=> __( 'Sitemap', 'w10' ),
			'priority'	=> 4
		)
	);

	$wp_customize->add_setting(
		'sitemap_authors',
		array(
			'default'	=> FALSE
		)
	);

	$wp_customize->add_control(
		'sitemap_authors',
		array(
			'section'		=> 'w10_sitemap',
			'label'			=> __( 'Show Authors', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'sitemap_pages',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'sitemap_pages',
		array(
			'section'		=> 'w10_sitemap',
			'label'			=> __( 'Show Pages', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'sitemap_posts',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'sitemap_posts',
		array(
			'section'		=> 'w10_sitemap',
			'label'			=> __( 'Show Posts', 'w10' ),
			'type'			=> 'checkbox'
		)
	);

	$wp_customize->add_setting(
		'sitemap_custom_posts',
		array(
			'default'	=> TRUE
		)
	);

	$wp_customize->add_control(
		'sitemap_custom_posts',
		array(
			'section'		=> 'w10_sitemap',
			'label'			=> __( 'Show Custom Posts', 'w10' ),
			'type'			=> 'checkbox'
		)
	);
}
add_action( 'customize_register', 'w10_register_theme_customizer' );

/**
* Customizer Head
*
* @return Customizer content to be added to <head>
*/
function w10_customizer_head() {
	echo get_theme_mod( 'w10_header_javascript', '' );
}
add_action( 'wp_head', 'w10_customizer_head' );

/**
* Customizer Footer
*
* @return Customizer content to be added to the bottom of the document
*/
function w10_customizer_footer() {
	echo get_theme_mod( 'w10_footer_javascript', '' );
}
add_action( 'wp_footer', 'w10_customizer_footer' );
