<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
* Theme Support
*
*/
add_theme_support( 'post-thumbnails', array( 'post' ) );


/**
* Register custom post types
*
* https://codex.wordpress.org/Function_Reference/register_post_type
*/
function w10_custom_post_types() {
	register_post_type(
		'my_post',
		array(
			'labels' => array(
				'name'			=> __( 'My Post', 'w10' ),
				'singular_name'	=> __( 'My Post', 'w10' ),
				'all_items'		=> __( 'All Posts', 'w10' ),
				'not_found'		=> __( 'My Posts', 'w10' ),
				'add_new_item'	=> __( 'Add New Post', 'w10' ),
				'edit_item'		=> __( 'Edit Post', 'w10' ),
				'view_item'		=> __( 'View Post', 'w10' ),
			),
			'public'		=> TRUE,
			'rewrite'		=> array( 'slug' => 'my-posts' ),
			'menu_icon'		=> 'dashicons-lightbulb', // https://developer.wordpress.org/resource/dashicons/
			'supports'		=> array( 'title', 'editor', 'page-attributes' ),
			'has_archive'	=> TRUE,
		)
	);
}
// Uncomment this to register custom post types...
// add_action( 'init', 'w10_custom_post_types' );


/**
* Register custom taxonomies
*
* https://codex.wordpress.org/register_taxonomy
*/
function w10_custom_taxonomies() {
	register_taxonomy(
		'my_taxonomy',
		'my_post',
		array(
			'labels' => array(
				'name'				=> __( 'Taxonomy', 'w10' ),
				'singular_name'		=> __( 'Taxonomy', 'w10' ),
				'search_items'		=> __( 'Search Taxonomy', 'w10' ),
				'all_items'			=> __( 'All Taxonomies', 'w10' ),
				'not_found'			=> __( 'No terms found.', 'w10' ),
				'parent_item'		=> __( 'Parent Taxonomy', 'w10' ),
				'parent_item_colon'	=> __( 'Parent Taxonomy:', 'w10' ),
				'update_item'		=> __( 'Update Taxonomy', 'w10' ),
				'edit_item'			=> __( 'Edit Taxonomy', 'w10' ),
				'add_new_item'		=> __( 'Add New Taxonomy', 'w10' ),
				'new_item_name'		=> __( 'New Taxonomy Name', 'w10' ),
				'menu_name'			=> __( 'Taxonomy', 'w10' ),
			),
			'hierarchical'		=> TRUE,
			'show_ui'			=> TRUE,
			'show_in_nav_menus'	=> TRUE,
			'public'			=> TRUE,
			'show_admin_column'	=> TRUE,
			'query_var'			=> TRUE,
			'rewrite'			=> array('slug' => 'my-taxonomy'),
		)
	);
}
// Uncomment this to register custom post types...
//add_action( 'init', 'w10_custom_taxonomies' );


/**
* Get page meta description
*
* @param post ID
*
* @return string meta description
*/
function get_metadesc( $post_id = 0 ) {
	$metadesc = get_post_custom_values( '_yoast_wpseo_metadesc', $post_id );
	return $metadesc[0];
}
