<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Register menus
function w10_register_my_menus() {
	register_nav_menus(
		array(
			'header-menu' => __( 'Header Menu', 'w10' ),
			'footer-menu' => __( 'Footer Menu', 'w10' ),
		)
	);
}
add_action( 'init', 'w10_register_my_menus' );


// Remove active classes from custom menu items to prevent multiple active links
function remove_menu_item_classes( $classes, $item ) {
	if ( in_array( 'menu-item-type-custom', $classes ) ) {
		$remove = array( 'current-menu-item', 'current_page_item' );
		$classes = array_diff( $classes, $remove );
	}
	return $classes;
}
add_filter( 'nav_menu_css_class', 'remove_menu_item_classes', 10, 2 );
