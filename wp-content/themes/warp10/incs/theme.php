<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Set up some additional image sizes
add_image_size( 'hero', 2000, 960, TRUE );
add_image_size( 'gallery', 1000, 750, TRUE );
add_image_size( 'gallery-thumbnail', 500, 375, TRUE );
add_image_size( 'link-panel', 600, 500, TRUE );
add_image_size( 'header', 2000, 500, TRUE );


// Load styles
function w10_load_styles() {
	wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/dist/css/main.css', SITE_VERSION );
}

add_action( 'wp_enqueue_scripts', 'w10_load_styles', 10 );


// Load scripts
function w10_load_scripts() {
	wp_enqueue_script( 'helpers', get_stylesheet_directory_uri() . '/js/helpers.js', '', SITE_VERSION, TRUE );
	wp_enqueue_script( 'blocks', get_stylesheet_directory_uri() . '/js/blocks.js', '', SITE_VERSION, TRUE );
	if ( get_theme_mod( 'fixed_sidebar', FALSE ) ) {
		wp_enqueue_script( 'sidebar', get_stylesheet_directory_uri() . '/js/sidebar.js', '', SITE_VERSION, TRUE );
	}
	wp_enqueue_script( 'main', get_stylesheet_directory_uri() . '/js/main.js', '', SITE_VERSION, TRUE );
}

add_action( 'wp_enqueue_scripts', 'w10_load_scripts', 10 );


// Custom Logo
function theme_prefix_setup() {
    add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'theme_prefix_setup' );


// Load more posts ajax
function w10_load_posts() {
	global $wp_query;

	wp_enqueue_script( 'w10-load-posts', get_template_directory_uri() . '/js/load-posts.js', 'jquery', SITE_VERSION, TRUE );

	$max = $wp_query->max_num_pages;
	$paged = ( get_query_var('paged') > 1 ) ? get_query_var( 'paged' ) : 1;

	wp_localize_script( 'w10-load-posts', 'w10LoadPosts', array( 'startPage' => $paged, 'maxPages' => $max, 'nextLink' => next_posts( $max, FALSE ) ) );
}
add_action( 'template_redirect', 'w10_load_posts' );


// Register sidebars
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name'			=> __( 'Sidebar', 'w10' ),
		'id'			=> 'sidebar',
		'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<span class="sidebar-title">',
		'after_title'	=> '</span>'
	));
}


// Custom comment template
function w10_comment($comment, $args, $depth) {
	if ( 'div' === $args['style'] ) {
		$tag		= 'div';
		$add_below	= 'comment';
	} else {
		$tag		= 'li';
		$add_below	= 'div-comment';
	} ?>
	<<?php echo $tag; ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID() ?>">
	<?php endif; ?>
		<div class="comment-meta commentmetadata">
			<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>"><?php printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?></a>
			<?php edit_comment_link( __( '(Edit)' ), '  ', '' ); ?>
		</div>
		<div class="comment-author vcard">
			<?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
			<?php printf( __( '<span>%s says:</span>' ), get_comment_author_link() ); ?>
		</div>
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
			<br />
		<?php endif; ?>
		
		<div class="comment-body">
			<?php comment_text(); ?>
		</div>

		<div class="comment-reply">
			<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div>
	<?php if ( 'div' != $args['style'] ) : ?>
		</div>
	<?php endif; ?>
<?php }
