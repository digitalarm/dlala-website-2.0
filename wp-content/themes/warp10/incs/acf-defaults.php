<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( function_exists('acf_add_options_page') ) {
		
	acf_add_options_page(array(
		'page_title' 	=> 'Template',
		'menu_title'	=> 'Template',
		'menu_slug' 	=> 'options',
		'capability'	=> 'edit_posts',
		'redirect'		=> FALSE
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Social Media',
		'menu_title'	=> 'Social Media',
		'parent_slug'	=> 'options',
	));

}
