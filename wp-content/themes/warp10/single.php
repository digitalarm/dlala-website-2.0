<?php
get_header(); 
?>

<div class="page-header-default">
	<div class="header-image">
		<?php if ( get_field ( 'header_image' ) ) :
			$image = get_field ( 'header_image' );
		else: 
			$image = get_field ( 'header_image', 'option' );
		endif; ?>

		<img src="<?php echo $image['sizes']['header']; ?>" alt="" >

	</div>
	
	<a class="header-title" href="<?php the_permalink( 389 ); ?>"><h1>News &amp; Blog</h1></a>
</div>

<div class="spacing-inside">
	<div class="container">
		<div class="page-content">
			<?php while ( have_posts() ) : the_post(); ?>
				<article class="article-page" id="post-<?php the_ID(); ?>">
					<div class="article-top">
					<div class="article-info">
							<span class="article-date"><?php printf( __( 'Posted on %s in ', 'w10' ), get_the_time( 'd M Y' ) ); the_category( ', ' ); echo $author; ?></span>
							<h2 class="article-title"><?php the_title(); ?></h2>
							
						</div>
						<?php if ( has_post_thumbnail() ) : $url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' ); ?>
							<img src="<?php echo $url[ 0 ]; ?>"></img>
						<?php endif; ?>
						<?php if ( TRUE === get_theme_mod( 'w10_author_image', FALSE ) ) : ?>
							<div class="article-author">
								<?php echo get_avatar( get_the_author_meta( 'ID' ), 96 ); ?>
								<?php if ( TRUE === get_theme_mod( 'w10_author_name', FALSE ) ) : ?>
									<span><?php printf( __( 'by %s' ), get_the_author() ); ?></span>
								<?php endif; ?>
							</div>
						<?php elseif ( TRUE === get_theme_mod( 'w10_author_name', FALSE ) ) : ?>
							<?php $author = sprintf( __( ' by %s' ), get_the_author() ); ?>
						<?php endif; ?>
						
					</div>
					<div class="article-text"><?php the_content(); ?></div>
				</article>
				<?php if ( TRUE === get_theme_mod( 'enable_comments', TRUE ) ) :
					comments_template();
				endif; ?>
			<?php endwhile; ?>
		</div>
	</div>
</div>

<?php
get_footer();
?>
