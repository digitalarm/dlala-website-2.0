<?php
/**
* The template for displaying search results pages
*
* @package WordPress
* @subpackage Twenty_Sixteen
* @since Twenty Sixteen 1.0
*/

get_header(); ?>

<div class="block block-blog-posts spacing-inside">
	<div class="container">
		<h1><?php printf( __( 'Search results for: %s', 'w10' ), esc_html( get_search_query() ) ); ?></h1>
		<?php get_search_form(); ?>
		<?php get_sidebar(); ?>
		<div class="blog-posts page-content">
			<?php if ( have_posts() ) : ?>
				<div class="grid">
					<div class="grid-sizer"></div>
					<?php while ( have_posts() ) : the_post();
						get_template_part( 'content', 'search' );
					endwhile; ?>
				</div>
				<div class="pagination">
					<?php posts_nav_link( ' ', '<span class="fa fa-minus-circle"></span> Prev', '<span class="fa fa-plus-circle"></span> Next' ); ?>
					<button class="load-posts"><span class="fa fa-plus-circle"></span> LOAD MORE POSTS</button>
				</div>
			<?php else : ?>
				<p><?php _e( 'Sorry, no posts could be found. Please try again using different keywords.', 'w10' ); ?></p>
				<?php get_search_form(); ?>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
