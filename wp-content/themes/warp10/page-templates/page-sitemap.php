<?php
/**
 * Template Name: Sitemap
 *
 * Description: Sitemap page template.
 *
 */

get_header();
?>

<div class="page-header-default">
	<div class="header-image">
		<?php $image = get_field ( 'header_image' ); ?>
		<img src="<?php echo $image['sizes']['header']; ?>" alt="" >

	</div>
	
	<div class="header-title"><h1><?php the_title(); ?></h1></div>
</div>

<div class="spacing-inside">
	<div class="container aligncenter sitemap">

		<?php if ( TRUE === get_theme_mod( 'sitemap_pages', TRUE ) ) : ?>
			<h2><?php _e( 'Pages', 'w10' ); ?></h2>
			<ul>
				<?php // Add pages you'd like to exclude in the exclude here
				wp_list_pages(
					array(
						'exclude' => '',
						'title_li' => ''
					)
				); ?>
			</ul>
		<?php endif; ?>
	</div>
</div>
	
<?php get_footer(); ?>
