<?php
/**
 * Template Name: Home
 *
 * Description: Home page template.
 *
 */

get_header(); 
?>

<div class="block block-hero-slider spacing-<?php the_sub_field( 'spacing' ); ?> ">
	<div class="image-slider">
		<div class="image-slides">
		<?php while ( have_rows( 'slides' ) ) : the_row();
			$image = get_sub_field( 'image' ); ?>
			<div class="slick-slide hero-slide">
				<div class="hero-slide-container">
					
					<img src="<?php echo $image['sizes']['hero']; ?>" alt="<?php echo $image['alt']; ?>">
					
					<?php if ( get_sub_field ( 'game' ) ) : ?>
						<?php if ( get_sub_field( 'logo' ) ) : $logo = get_sub_field ( 'logo' ); ?>
							<div class="slide-game-logo">
								<a href="<?php the_sub_field ( 'game_link' ); ?>" ><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"></a>
							</div>
						<?php endif; ?>
						<div class="hero-store-links">
						<?php if ( get_sub_field( 'steam_store_link' ) ) : ?>
							<a href="<?php the_sub_field ( 'steam_store_link'); ?>" target="_blank" class="hero-store-link">
								<img src="<?php bloginfo( 'template_directory' ); ?>/dist/img/store-steam.png" />
							</a>
						<?php endif; ?>
						<?php if ( get_sub_field( 'xbox_store_link' ) ) : ?>
							<a href="<?php the_sub_field ( 'xbox_store_link'); ?>" target="_blank" class="hero-store-link">
								<img src="<?php bloginfo( 'template_directory' ); ?>/dist/img/store-xbox.png" />
							</a>
						<?php endif; ?>
						<?php if ( get_sub_field( 'ps4_store_link' ) ) : ?>
							<a href="<?php the_sub_field ( 'ps4_store_link'); ?>" target="_blank" class="hero-store-link">
								<img src="<?php bloginfo( 'template_directory' ); ?>/dist/img/store-ps4.png" />
							</a>
						<?php endif; ?>
					<?php else: ?>
						<div class="hero-text-container">
							<div class="hero-text"><?php the_sub_field ( 'text' ); ?></div>
						</div>
					<?php endif; ?>
					

				</div>
			</div>
		<?php endwhile; ?>
		</div>
	</div>
</div>


<?php get_template_part( 'content', 'layout-blocks' ); ?>

<div id="clients" class="block block-clients spacing-inside">
	<div class="block-title aligncenter">Who we have worked with</div>
	<div class="client-slider-nav">
		<div class="nav-slides">
			<?php  while ( have_rows( 'clients' ) ) : the_row();
				$image = get_sub_field( 'logo' ); ?>
				<div class="client-slide ">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
				</div>
			<?php endwhile; ?>
		</div>
	</div>
	<div class="container">
		<div class="client-slider-with-nav">
			<div class="image-slides">			
				<?php  while ( have_rows( 'clients' ) ) : the_row(); ?>
					<div class="slick-slide " >
						<div class="client-testimonial">
							<?php the_sub_field ( 'quote' ); ?>
							<br><span class="client-testimonial-author"><strong><?php the_sub_field ( 'quote_author' ); ?></strong></span>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</div>

<?php 
	$args = array(
	'post_type' => 'post',

	'orderby' => 'date',
	'order' => 'DESC'
);
$posts = get_posts( $args );

foreach( $posts as $post ) : setup_postdata( $post ); ?>
	<?php if ( get_field ( 'featured' ) ) : ?>
		<div class="home-post bg-grey">
			<div class="container">
				<div class="post-grid">
					<?php if ( has_post_thumbnail() ) : ?>
					<div class="post-grid__item post-grid__item--thumbnail">
						<?php the_post_thumbnail( $post->ID, 'large' ); ?>
					</div>
					<?php endif; ?>
					<div class="post-grid__item post-grid__info">
						<span class="home-post-title"><?php echo $post->post_title; ?></span>
						<div class="latest-post-excerpt"><?php the_excerpt(); ?></div>
						<a href="<?php echo get_permalink( $post->ID ); ?>"><?php _e( 'Read post', 'w10' ); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php endforeach; wp_reset_postdata(); ?>
	
<?php get_footer(); ?>
