<?php
/**
 * Template Name: Search
 *
 * Description: Search page template.
 *
 */

get_header();
?>

<div class="search-block spacing-inside">
	<div class="container">
		<h1><?php the_title(); ?></h1>
		<?php get_sidebar(); ?>
		<div class="page-content">
			<?php get_search_form(); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
