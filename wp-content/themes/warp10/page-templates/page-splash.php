<?php
/**
 * Template Name: Splash
 *
 * Description: Splash page template.
 *
 */

get_header();
?>

<?php get_template_part( 'content', 'layout-blocks' ); ?>
	
<?php get_footer(); ?>
