<?php
/**
 * Template Name: Contact
 *
 * Description: Contact page template.
 *
 */

get_header();
?>

<div class="page-header-default">
	<div class="header-image">
		<?php $image = get_field ( 'header_image' ); ?>
		<img src="<?php echo $image['sizes']['header']; ?>" alt="" >

	</div>
	
	<div class="header-title"><h1><?php the_title(); ?></h1></div>
</div>

<div class="contact-block">
	<div class="grid">
		<div class="column-50">
			<?php $image = get_field ( 'image' ); ?>
			<div class="contact-content bg-white"><?php the_field ( 'content' ); ?></div>
			<div class="contact-image cover" style="background-image: url(<?php echo $image['url']; ?>);"></div>
		</div>
		<div class="column-50 bg-primary contact-form-container aligncenter wow slideInRight" data-wow-delay=".2s" >
			<?php the_field( 'contact_form' ); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
