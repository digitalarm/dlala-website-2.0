<?php
/**
 * Template Name: Team
 *
 * Description: Team page template.
 *
 */

get_header(); 
?>

<div class="page-header-default">
	<div class="header-image">
		<?php $image = get_field ( 'header_image' ); ?>
		<img src="<?php echo $image['sizes']['header']; ?>" alt="" >

	</div>
	
	<div class="header-title"><h1><?php the_title(); ?></h1></div>
</div>

<div class="team-block">
	<?php while( has_sub_fields( 'team_member' ) ) : ?>
		<?php 
		$image = get_sub_field ( 'image' ); 
		?>

		<div class="block block-two-column-text-image">
			<div class="container bg-grey">
			<span class="two-column-image two-column-image-<?php echo $image_class; ?> cover team-two-column-image-<?php echo $image_class; ?> wow" style="background-image: url(<?php echo $image['url']; ?>);"></span>
				<div class="two-column-text bg-grey two-column-text-<?php echo $text_class; ?> spacing-inside team-two-column-text-<?php echo $text_class; ?>">
					<div class="team-name"><?php the_sub_field('name'); ?></div>
					<div class="team-job"><?php the_sub_field('job_title'); ?></div>
					
					<div class="team-details">
						
						<?php if( ! get_sub_field ( 'web_team' ) ) : ?>
							<div class="top-5-games">
								<h3>Top 5 Games</h3>
								<?php while( has_sub_fields ( 'top_5_games' ) ) : ?>
									<span class="team-game"><?php the_sub_field('game'); ?></span>
								<?php endwhile; ?>
							</div>
							<div class="team-text">
								<h3>Background</h3>
								<?php the_sub_field('background'); ?>
								<h3>Typical Day</h3>
								<?php the_sub_field('typical_day'); ?>
								<h3>If You Could Be Any Video Game Character, Who and Why?</h3>
								<?php the_sub_field('game_character'); ?>
							</div>
						<?php else: ?>
							<div class="web-team-details">
								<h3>Background</h3>
								<?php the_sub_field('background'); ?>
								<h3>Typical Day</h3>
								<?php the_sub_field('typical_day'); ?>
								<h3>Free time</h3>
								<?php the_sub_field('free_time'); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			
		</div>
	</div>
	<?php endwhile; ?>
</div>

<?php get_template_part( 'content', 'layout-blocks' ); ?>

	
<?php get_footer(); ?>
