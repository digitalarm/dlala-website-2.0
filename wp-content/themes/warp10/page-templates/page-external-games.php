<?php
/**
 * Template Name: External Games
 *
 * Description: External Game page template.
 *
 */

get_header(); 
?>

<div class="page-header-default">
	<div class="header-image">
		<?php if ( get_field ( 'header_image' ) ) :
			$image = get_field ( 'header_image' );
		else: 
			$image = get_field ( 'header_image', 'option' );
		endif; ?>

		<img src="<?php echo $image['sizes']['header']; ?>" alt="" >

	</div>
	<div class="header-title"><h1><?php the_title(); ?></h1></div>
</div>

<div class="block block-external-games">
	<?php while ( has_sub_fields ( 'games' ) ) : ?>
		<div class="external-game">
			<div class="external-game__left spacing-inside">
				<?php
				$gameLogo = get_sub_field ( 'logo' );
				$publisher = get_sub_field ( 'publisher_logo' );
				?>
				<div class="external-game__logo"><img src="<?php echo $gameLogo['url']; ?>" alt="<?php echo $gameLogo['title']; ?>" ></div>
				<div class="external-game__description"><?php the_sub_field ( 'description' ); ?></div>
				<div class="external-game__publisher"><img src="<?php echo $publisher['url']; ?>" alt="<?php echo $publisher['title']; ?>" ></div>
			</div>
			<div class="external-game__right spacing-inside">
			
				<div class="external-game__our-work">
					<div class="external-game__work-title block-title">Our Work</div>
					<?php the_sub_field ( 'our_work' ); ?>
				</div>

				<?php if ( get_sub_field ( 'awards' ) ) : ?>
					<div class="external-awards">
						<h3>Awards</h3>
						<?php the_sub_field ( 'awards' ); ?>
					</div>
				<?php endif; ?>

				<?php if ( get_sub_field ( 'reviews' ) ) : ?>
					<div class="external-game__reviews">
						<h3>Reviews</h3>
						<?php while ( has_sub_fields('reviews') ) : ?>
							<div class="external-game__review"><?php the_sub_field ( 'review' ); ?></div>
							<div class="external-game__author"><?php the_sub_field ( 'author' ); ?></div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>


	<?php endwhile; ?>
</div>

<?php if ( have_rows ( 'clients' ) ) : ?>
<div id="clients" class="block block-clients spacing-inside">
	<div class="block-title aligncenter">Who we have worked with</div>
	<div class="client-slider-nav">
		<div class="nav-slides">
			<?php  while ( have_rows( 'clients' ) ) : the_row();
				$image = get_sub_field( 'logo' ); ?>
				<div class="client-slide ">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
				</div>
			<?php endwhile; ?>
		</div>
	</div>
	<div class="container">
		<div class="client-slider-with-nav">
			<div class="image-slides">			
				<?php  while ( have_rows( 'clients' ) ) : the_row(); ?>
					<div class="slick-slide " >
						<div class="client-testimonial">
							<?php the_sub_field ( 'quote' ); ?>
							<br><span class="client-testimonial-author"><strong><?php the_sub_field ( 'quote_author' ); ?></strong></span>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

	
<?php get_footer(); ?>
