<?php
/**
 * Template Name: Game
 *
 * Description: Game page template.
 *
 */

get_header(); 
?>

<div class="page-header">
	<div class="header-image">
		<?php $image = get_field ( 'header_image' ); ?>
		<img class="game-header-image" src="<?php echo $image['sizes']['header']; ?>" alt="" >
		<?php if ( get_field( 'game_logo' ) ) : $logo = get_field ( 'game_logo' ); ?>
			<div class="slide-game-logo <?php if ( ! get_field( 'ps4_store_link' ) OR get_field( ! 'xbox_store_link' ) OR get_field( ! 'steam_store_link' ) ) : ?>game-logo-padding<?php endif;?> wow zoomIn" data-wow-delay=".2s">
				<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">	
			</div>
		<?php endif; ?>
		<?php if ( get_field( 'ps4_store_link' ) OR get_field( 'xbox_store_link' ) OR get_field( 'steam_store_link' ) ) : ?>
		<div class="store-links-container wow fadeInUp" data-wow-delay=".2s"">
			<div class="hero-store-links">
				<?php if ( get_field( 'steam_store_link' ) ) : ?>
					<a href="<?php the_field ( 'steam_store_link'); ?>" target="_blank" class="hero-store-link">
						<img src="<?php bloginfo( 'template_directory' ); ?>/dist/img/store-steam.png" />
					</a>
				<?php endif; ?>
				<?php if ( get_field( 'xbox_store_link' ) ) : ?>
					<a href="<?php the_field ( 'xbox_store_link'); ?>" target="_blank" class="hero-store-link">
						<img src="<?php bloginfo( 'template_directory' ); ?>/dist/img/store-xbox.png" />
					</a>
				<?php endif; ?>
				<?php if ( get_field( 'ps4_store_link' ) ) : ?>
					<a href="<?php the_field ( 'ps4_store_link'); ?>" target="_blank" class="hero-store-link">
						<img src="<?php bloginfo( 'template_directory' ); ?>/dist/img/store-ps4.png" />
					</a>
				<?php endif; ?>
			</div>
		</div>
		<?php endif; ?>
	</div>
</div>

<?php get_template_part( 'content', 'layout-blocks' ); ?>

	
<?php get_footer(); ?>
