<?php
get_header(); 
?>

<div class="page-header-default">
	<div class="header-image">
		<?php $image = get_field ( 'header_image', 355 ); ?>
		<img src="<?php echo $image['sizes']['header']; ?>" alt="" >

	</div>
	
	<div class="header-title"><h1><?php the_title(); ?></h1></div>
</div>

<div class="block-404 spacing-inside">
	<div class="container">
		<h1><?php _e( '404 Page not found', 'w10' ); ?></h1>
		<p><?php _e( 'Sorry, that page could not be found. Please use the menu to find what you\'re looking for.', 'w10' ); ?></p>
	</div>
</div>

<?php
get_footer();
?>
