/* global require */

var gulp			= require( 'gulp' );
var del				= require( 'del' );
var postcss			= require( 'gulp-postcss' );
var scss			= require( 'postcss-scss' );
var reporter		= require( 'postcss-reporter' );
var stylelint		= require( 'stylelint' );
var sourcemaps		= require( 'gulp-sourcemaps' );
var sass			= require( 'gulp-sass' );
var autoprefixer	= require( 'autoprefixer' );
var eslint			= require( 'gulp-eslint' );
var imagemin		= require( 'gulp-imagemin' );
var browsersync		= require( 'browser-sync' ).create();

var paths = {
	'scss': [
		'scss/*.scss',
		'scss/*/*.scss',
	],
	'js': 'js/*.js',
	'php': [
		'*.php',
		'*/*.php',
	],
	'img': 'img/*',
};

/* gulp.task( 'default', ['clean:css', 'clean:img', 'css', 'img', 'js'], function() {

});*/

gulp.task( 'clean:css', function() {

	return del( ['dist/css'] );

});

gulp.task( 'clean:img', function() {

	return del( ['dist/img'] );

});

gulp.task( 'css', ['clean:css'], function() {

	return gulp.src( paths.scss )
		.pipe(
			sourcemaps.init()
		)
		.pipe(
			postcss( [
				stylelint(),
				reporter({ 'clearMessages': true /* throwError: true*/}),
			], { 'syntax': scss })
		)
		.pipe(
			sass({
				'errLogToConsole': true,
				'indentType': 'tab',
				'indentWidth': 1,
				'outputStyle': 'expanded',
			})
		)
		.pipe(
			postcss( [
				autoprefixer({ 'browsers': ['> 1%', 'last 2 versions', 'ie >= 9'] }),
			] )
		)
		.pipe(
			sourcemaps.write( '' )
		)
		.pipe(
			gulp.dest( 'dist/css' )
		)
		.pipe(
			browsersync.stream()
		);

});

gulp.task( 'img', ['clean:img'], function() {

	return gulp.src( paths.img )
		.pipe(
			imagemin()
		)
		.pipe(
			gulp.dest( 'dist/img' )
		);

});

gulp.task( 'img-watch', ['img'], function( done ) {

	browsersync.reload();
	done();

});

gulp.task( 'js', function() {

	return gulp.src( paths.js )
		.pipe(
			eslint({ 'fix': true })
		)
		.pipe(
			eslint.format()
		)
		.pipe(
			eslint.failAfterError()
		)
		.pipe(
			gulp.dest( 'js' )
		);

});

gulp.task( 'js-watch', ['js'], function( done ) {

	browsersync.reload();
	done();

});

gulp.task( 'watch', ['css', 'img', 'js'], function() {

	gulp.watch( paths.scss, ['css'] );
	gulp.watch( paths.img, ['img'] );
	gulp.watch( paths.js, ['js'] );

});

gulp.task( 'bs', ['css', 'img', 'js'], function() {

	browsersync.init({
		'proxy': 'warp10.local',
		//host: '',
	});

	gulp.watch( paths.scss, ['css'] );
	gulp.watch( paths.img, ['img-watch'] );
	gulp.watch( paths.js, ['js-watch'] );
	gulp.watch( paths.php ).on( 'change', browsersync.reload );

});
