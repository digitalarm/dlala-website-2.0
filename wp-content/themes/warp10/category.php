<?php
get_header(); 
?>

<div class="page-header-default">
	<div class="header-image">
		<?php $image = get_field ( 'header_image' ); ?>
		<img src="<?php echo $image['sizes']['header']; ?>" alt="" >

	</div>
	
	<a class="header-title" href="<?php the_permalink( 389 ); ?>">
		<h1>
			<?php if ( get_option( 'page_for_posts' ) == 0 ) : ?>
				<?php $page_title = single_cat_title( '', FALSE ); ?>
			<?php else: ?>
				<?php $page_title = apply_filters( 'the_title', get_page( get_option( 'page_for_posts' ) )->post_title ) . single_cat_title( ' - ', FALSE ); ?>
			<?php endif; ?>
			<?php echo $page_title; ?>
		</h1>
	</a>
</div>


<div class="block block-blog-posts spacing-inside">
	<div class="container">

		<div class="blog-posts page-content">
			<div class="grid">
				<div class="grid-sizer"></div>
				<?php while ( have_posts() ) : the_post();
					get_template_part( 'content' );
				endwhile; ?>
			</div>
			<div class="pagination">
				<?php posts_nav_link( ' ', '<span class="fa fa-minus-circle"></span> Prev', '<span class="fa fa-plus-circle"></span> Next' ); ?>
				<button class="load-posts"><span class="fa fa-plus-circle"></span> LOAD MORE POSTS</button>
			</div>
		</div>
	</div>
</div>

<?php
get_footer();
?>
