/************************************

Dlala Studios Patch Notes

************************************/

Version 1.0.1 - updated 11/08/17
-----------------------------

Headline Changes
----------------
- Altered blog post layout on homepage
- Added External Games page template
- Added 'Our Games' parent page to games section
- Removed blog auto-centralisation
- Team page now auto-sets layout and background colours




Version 1.0.0 - updated 03/07/17
-----------------------------

Headline Changes
----------------
Initial site build


- 
