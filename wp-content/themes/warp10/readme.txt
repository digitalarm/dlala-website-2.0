/************************************

Warp10 Setup

************************************/

NOTE: This article assumes you already have WordPress installed.

- In WordPress go to Appearance > Themes > Add New (upload Warp10.zip) and activate it.

- Plugins

(Required) Install Advanced TinyMCE plugin: https://wordpress.org/plugins/tinymce-advanced/
Download and import these settings: TinyMCE-advanced-settings.
(Required) ACF plugin. Download here: https://drive.google.com/open?id=0B-8c94tBI-HuTk1CTmhOVTFnTVU
Once installed, go to Custom Fields > Updates and paste in this key: b3JkZXJfaWQ9NTY2MDd8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE1LTA1LTIyIDAwOjA2OjI5
(Recommended) Install Yoast SEO: https://wordpress.org/plugins/wordpress-seo/
(Required if Yoast SEO is installed) Install ACF Content Analysis for SEO
(Recommended) Install Autoptimize: https://wordpress.org/plugins/autoptimize/
(Recommended) Install Engage: https://wordpress.org/plugins/engage-forms/

- Import ACF Layout Blocks via Custom Fields > Import. The JSON file for this is located in the Warp10 theme's imports folder.

- Change theme name in style.css from Warp10 to site name.

- Edit settings file (incs/settings.php) and turn on / off required settings.

That’s it, the basic set up is complete.



/************************************

Libraries

************************************/

Warp10 comes with the following libraries packaged, some are enabled by default, others must be enabled to use.

Animate.css (default : FALSE)
FontAwesome (default : TRUE)
Google Maps (default: FALSE)
Hoverizr (default: FALSE)
Isotope (default: TRUE)
Lazyload (default: FALSE)
Magnific Popup (default: TRUE)
Modernizr (default: TRUE) - comes with media queries and HTML5 polyfill. Feel free to build your own and add features https://modernizr.com/
Normalize.css (default: TRUE)
Slick (default: TRUE)
Wait For Images (default: TRUE)
Wow js (default : FALSE – requires animate.css)
You can manage these libraries via the incs/settings.php file.



/************************************

Settings

************************************/

Other settings are available via the incs/settings.php including:

Disable comments (default : TRUE)
Disable pingbacks (default : TRUE)
Mockups module (default : FALSE)
Sitemap settings



/************************************

Templates

************************************/

Warp10 comes with some default templates, layouts and CSS.

Located in page-templates.

Home
Sitemap
Splash

Located in page-layout-blocks.

Single column text
Single column image
Single column video
Two column text
Two column text image
Three column text
Text slider
Image slider
Video slider
Latest posts
Google map
Collapsible content
Collapsible sectioned text
Parallax



/************************************

CSS

************************************/

We don’t use style.css for any styles, this is used for theme declaration only.

default.css – this is used to form the base version of Warp10. The classes in here can be overridden in site.css if required. 
default-responsive.css – used for smaller desktop, tablet and mobile views on the base version of Warp10. The classes in here can be overridden in site-responsive.css if required.
site.css – used for the desktop view of the site, all layout and colours
site-responsive.css – used for smaller desktop, tablet and mobile views, filled with some pre set media queries to get you going.

Please do not edit either of the default stylesheets as you run the risk of any changes being overwritten at a later date.
