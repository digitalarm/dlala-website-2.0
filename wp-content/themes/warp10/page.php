<?php
get_header(); 
?>

<div class="page-header-default">
	<div class="header-image">
		<?php $image = get_field ( 'header_image' ); ?>
		<img src="<?php echo $image['sizes']['header']; ?>" alt="" >

	</div>
	
	<div class="header-title"><h1><?php the_title(); ?></h1></div>
</div>

<?php get_template_part( 'content', 'layout-blocks' ); ?>

<?php
get_footer();
?>